#include "Header.h"

int main()
{
	std::vector<char> vertices = { 'a', 'b', 'c', 'd', 'e', 'f' };
	std::vector<Edge> result;
	Graph graph;
	graph.vertices = vertices;
	
	graph.edges.push_back(Edge('a', 'b', 4));
	graph.edges.push_back(Edge('a', 'f', 2));
	graph.edges.push_back(Edge('f', 'b', 5));
	graph.edges.push_back(Edge('c', 'b', 6));
	graph.edges.push_back(Edge('c', 'f', 1));
	graph.edges.push_back(Edge('f', 'e', 4));
	graph.edges.push_back(Edge('d', 'e', 2));
	graph.edges.push_back(Edge('d', 'c', 3));

	result = Kruskal(graph);
	for (auto edge : result)
	{
		std::cout << edge.vertex1 << ", " << edge.vertex2 << " = " << edge.weight << "\n";
	}

	std::cout << "\n\n";

	result = Prim(graph);
	for (auto edge : result)
	{
		std::cout << edge.vertex1 << ", " << edge.vertex2 << " = " << edge.weight << "\n";
	}

	system("pause");
}

