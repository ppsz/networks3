#pragma once
#include <iostream>
#include <vector>
#include <map>
#include <algorithm>
#include <limits>

struct Edge
{
	char vertex1, vertex2;
	int weight;
	Edge(char v1, char v2, int w)
	{
		vertex1 = v1;
		vertex2 = v2;
		weight = w;
	}
};

struct Graph
{
	std::vector<char> vertices;
	std::vector<Edge> edges;
};

std::vector<Edge> Kruskal(Graph graph);
std::vector<Edge> Prim(Graph graph);