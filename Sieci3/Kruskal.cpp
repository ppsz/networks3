#include "Header.h"

std::map<char, char> g_parent;
std::map<char, int> g_rank;

inline bool Compare(Edge &a, Edge &b)
{
	return a.weight < b.weight;
}

void MakeSet(char vertex)
{
	g_parent[vertex] = vertex;
	g_rank[vertex] = 0;
}

char Find(char vertex)
{
	if (g_parent[vertex] != vertex)
		return Find(g_parent[vertex]);
	return g_parent[vertex];
}

void Union(char root1, char root2)
{
	if (g_rank[root1] > g_rank[root2])
		g_parent[root2] = root1;
	else if (g_rank[root1] < g_rank[root2])
		g_parent[root1] = root2;
	else
	{
		g_parent[root1] = root2;
		g_rank[root2]++;
	}
}

std::vector<Edge> Kruskal(Graph graph)
{
	std::sort(graph.edges.begin(), graph.edges.end(), Compare);
	std::vector<Edge> edges;
	for (auto &vertex : graph.vertices)
		MakeSet(vertex);
	for (auto edge : graph.edges)
	{
		char root1 = Find(edge.vertex1);
		char root2 = Find(edge.vertex2);

		if (root1 != root2)
		{
			edges.push_back(edge);
			Union(root1, root2);
		}
	}
	return edges;
}