#include "Header.h"

char Min(std::vector<char> &Q, std::map<char, int> &priority)
{
	char min = Q[0];
	for (auto vertex : Q)
		if (priority[min] > priority[vertex])
			min = vertex;
	return min;
}

std::vector< std::pair<char, Edge> > Adjacent(Graph &graph, char &u)
{
	std::vector< std::pair<char, Edge> > result;
	for (auto edge : graph.edges)
	{
		if (edge.vertex1 == u)
			result.push_back(std::pair<char, Edge>(edge.vertex2, edge));
		else if (edge.vertex2 == u)
			result.push_back(std::pair<char, Edge>(edge.vertex1, edge));
	}
	return result;
}

std::vector<Edge> Prim(Graph graph)
{
	std::vector<Edge> result;
	std::map<char, char> A;
	std::map<char, char> parent;
	std::map<char, int> priority;
	
	for (auto vertex : graph.vertices)
	{
		parent[vertex] = NULL;
		priority[vertex] = std::numeric_limits<int>::max();
	}
	priority[graph.vertices[0]] = 0; // Dowolny wierzcholek poczatkowy
	std::vector<char> Q = graph.vertices;

	while (!Q.empty())
	{
		char u = Min(Q, priority);
		Q.erase(remove(Q.begin(), Q.end(), u), Q.end());
		if (parent[u] != NULL)
			A[u] = parent[u];

		std::vector< std::pair<char, Edge> > adjacent_vertices = Adjacent(graph, u);
		for (auto &pair : adjacent_vertices)
		{
			if (find(Q.begin(), Q.end(), pair.first) != Q.end())
			{
				if (pair.second.weight < priority[pair.first])
				{
					parent[pair.first] = u;
					priority[pair.first] = pair.second.weight;
				}
			}
		}
	}
	for (auto &element : A)
	{
		for (auto &edge : graph.edges)
		{
			if (edge.vertex1 == element.first && edge.vertex2 == element.second ||
				edge.vertex2 == element.first && edge.vertex1 == element.second)
			{
				Edge temp(element.first, element.second, edge.weight);
				result.push_back(temp);
			}
		}
	}
	return result;
}